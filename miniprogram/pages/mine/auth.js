const app = getApp();
Page({
  data: {
    userInfo: {},
    showGetUserInfo:true,
    showGetPhoneNumber: true,
    showOpenSetting: true,
  },
  onLoad(option){
    if (option && option.setting){
      this.setData({
        showGetUserInfo:false,
        showGetPhoneNumber: false,
        showOpenSetting: true
      });
      wx.showToast({
        title: '请点击授权设置开启权限',
        icon: 'none',
        duration: 3000,
      })
    }
  },
  onGetUserInfo: function (e) {
    if (e.detail && e.detail.userInfo) {
      wx.showToast({
        title: '微信登录成功',
        duration: 2000,
      })
      console.log(e.detail.userInfo)
      //app.globalData.userInfo = e.detail.userInfo;
      setTimeout(function () {
        wx.switchTab({
          url: '/pages/mine/mine',
        })
       }, 2000);
    }
  },
  getPhoneNumber(e) {
    var cloudID = e.detail.cloudID;
    if (!cloudID) {
      wx.showToast({
        icon: "none",
        title: '登录失败',
        duration: 3000,
      })
      return;
    }
    //var _this = this;
    wx.cloud.callFunction({
      name: 'user',
      data: {
        action: 'getPhone',
        userPhone: wx.cloud.CloudID(cloudID), // 这个 CloudID 值到云函数端会被替换
        //systemInfo: app.globalData.systemInfo
      },
      success: res => {
        var userData = res.result;
        console.log(userData);
        app.globalData.userInfo = userData;
        wx.showToast({
          title: '手机登录成功',
          duration: 2000,
        })
        setTimeout(function () {
          wx.switchTab({
            url: '/pages/mine/mine',
          })
        }, 2000);
      },
      fail: err => {
        console.error('[云函数] [getPhone] 调用失败', err)
        wx.showToast({
          icon: "none",
          title: '登录失败:接口错误',
          duration: 3000,
        })
      }
    })
  },
  openSetting(){
    wx.openSetting()
  },
  onPullDownRefresh() {
    wx.stopPullDownRefresh();
    //reload
  }
});
