var app = getApp();
Page({
  data: {
    id:0,
    detail:{},
    isShowAuth:false,
    userInfo: app.globalData.userInfo
  },
  onLoad(options) {
    let id = options.id;
    this.setData({
      id: id
    });
    this.getDetail();
  },
  //访问网络,请求数据  
  getDetail() {
    var that = this;
    let id = this.data.id;
    if(!id){
      wx.showToast({
        icon: "none",
        title: '参数错误',
        duration: 3000,
      })
      return;
    }
    wx.showLoading({
      title: '数据加载中',
      mask: true,
    })
    wx.cloud.callFunction({
      name: 'pet',
      data: { action: 'getRecordDetail', id: this.data.id},
      success: res => {
        console.log('[云函数] [getRecordDetail] : ', res.result)
        if (res.result && res.result.code === 1){
          that.setData({
            detail: res.result.data,
          });
        }else{
          wx.showToast({
            icon: "none",
            title: res.result.msg,
            duration: 2000,
          })
        }
      },
      fail: err => {
        console.error('[云函数] [getRecordDetail] 调用失败', err)
        wx.showToast({
          icon: "none",
          title: "获取数据失败",
          duration: 2000,
        })
      },
      complete: () => {
        wx.hideLoading();
      }
    })
  },
  makeCall(){
    const bOpenid = this.data.detail._openid;
    var userInfo = app.globalData.userInfo || {};
    if (!userInfo.mobile) {
      wx.showToast({
        icon: 'none',
        title: '请先登录帐号',
        duration: 1000,
      })
      this.showAuthDialog();
      return;
    }
    wx.showLoading({
      title: '处理中',
      mask: true,
    })
    wx.cloud.callFunction({
      name: 'cmcc',
      data: { action: 'bindAXB', bOpenid: bOpenid },
      success: res => {
        console.log('[云函数] [bindAXB] : ', res.result)
        if (res.result && res.result.code == 1) {
          console.log(res.result.data)
          wx.makePhoneCall({
            phoneNumber: res.result.data
          })
        } else {
          wx.showToast({
            icon: 'none',
            title: res.result.msg,
            duration: 2000,
          })
        }
      },
      fail: err => {
        console.error('[云函数] [bindAXB] 调用失败', err)
      },
      complete: () => {
        wx.hideLoading();
      }
    })
    return;

  },
  delRecord(){
    wx.showLoading({
      title: '删除中',
      mask: true,
    })
    wx.cloud.callFunction({
      name: 'pet',
      data: { action: 'delRecord', id: this.data.id },
      success: res => {
        console.log('[云函数] [delRecord] : ', res.result)
        if (res.result && res.result.code === 1) {
          wx.showToast({ title: '删除成功', duration: 2000 })
          setTimeout(() => {
            wx.redirectTo({
              url: '/pages/record/record',
            })
          }, 1500);
        } else {
          wx.showToast({
            icon: "none",
            title: res.result.msg,
            duration: 2000,
          })
        }
      },
      fail: err => {
        console.error('[云函数] [delRecord] 调用失败', err)
        wx.showToast({
          icon: "none",
          title: "删除失败",
          duration: 2000,
        })
      },
      complete: () => {
        wx.hideLoading();
      }
    })
  },
  showLocation(e){
    let location = this.data.detail.location;
    if (location && location.latitude && location.longitude){
      location.scale = 18;
      wx.openLocation(location);
    }
  },
  viewImage(e){
    wx.previewImage({
      urls: this.data.detail.imgList
    });
  },
  showAuthDialog() {
    this.setData({
      isShowAuth: true
    })
  },
  onAuthEvent: function (e) {
    console.log('onAuthEvent', e);
    if (e.detail.mobile) {
      this.setData({
        userInfo: e.detail
      })
    }
  },
  onPullDownRefresh() {
    this.getDetail();
    wx.stopPullDownRefresh();
    //reload
  },
  onShareAppMessage() {
    const detail = this.data.detail;
    const path = `/pages/record/detail?id=${detail._id}&from=share`;
    const title = `我是留守宠物${detail.petName||''}，快来${detail.location.name||''}帮帮我`;
    const imageUrl = detail.imgList[0] || '/images/share.jpg';
    const db = wx.cloud.database();
    const shareLog = {
      petid: detail._id,
      createTime: new Date().getTime()
    };
    db.collection('share').add({
      data: shareLog
    })
    return {
      title: title,
      path: path,
      imageUrl: imageUrl
    }
  },
});
